import pandas as pd

def normalize(columns):
    ''' columns = {column_name: pandas_column_datatype_as_string, ...} '''
    ''' columns = {'FIRST NAME': 'string', 'score': 'int64'... } '''

    # dictionary with {column_name:type}
    conversion = {'Int64': 'NUMBER', 'string': 'VARCHAR'}

    result = dict()

    for cn, dt in columns.items():
        # convert spaces in column names to underscores '_'; "first name" -> "FIRST_NAME"
        cn = "_".join(str(cn).upper().split())

        # convert pandas datatypes to snowflake; Int64 -> "NUMBER"
        # later add max length to the conversion

        result[cn] = conversion[str(dt)]
    return result


def read_csv_dict(file_path):
    table = pd.read_csv(file_path, header=0)

    table = table.convert_dtypes(infer_objects=True, convert_string=True, convert_integer=True, convert_boolean=True)

    table_types = table.dtypes.astype(str).to_dict()

    return table_types

# convert path to windows compatible
def path_conversion(file_path):
    file_path.replace()
