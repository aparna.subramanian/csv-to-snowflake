import pandas as pd

from snowflakeConnection import sfconnect

from utility import normalize, read_csv_dict
# import numpy as np
# import snowflake

# Path of the csv file to load into snowflake
file_path = "C:\\Users\\Hashmap Admin\\Downloads\\currencies.csv"
print(file_path)
# set snowflake environment
cnx = sfconnect()
cur = cnx.cursor()

# Table name to create
TName = "CURRENCIES"

# read csv file to get the column names and their types for snowflake
table_types = normalize(read_csv_dict(file_path))

# create table in snowflake
DDL = "CREATE OR REPLACE TABLE " + TName + "( "
DDL += ", ".join([k + " " + v for k,v in table_types.items()])
DDL += ");"
print(DDL)

cur.execute(DDL)

# put the csv data to "table stage"
put = "PUT "\
      "'file://c:\\\\Users\\\\Hashmap Admin\\\\Downloads\\\\currencies.csv' " \
      "@%" + TName + " ;"
print(put)
cur.execute(put)
copy = "COPY INTO " + TName + " " \
                              "FILE_FORMAT = (TYPE = CSV " \
                              "SKIP_HEADER = 1);"
print(copy)
cur.execute(copy)
# print({c: table[c].dt for c in table.columns})

#print({a.Name: a.dtype for a in table.head(1)})




cur.close()
cnx.close()