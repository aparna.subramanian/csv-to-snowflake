
from snowflake import connector

def sfconnect():
    cnx = connector.connect(
        account="",
        user="",
        password="",
        database='CSV_DDL_DB',
        schema='PYTHON_CONNECTOR',
        warehouse='CONNECTION_OPS',
        role='SYSADMIN'
    )
    return cnx
